//OpenHackinToolBoxProject
//项目负责人 lajiaoyou
//仓库管理人 hehe1008899
//原作者 hehe1008899
//创作团队 FbTStudio
//协议 BSD
//请不要进行商业行为等行为,传播时请标明原作者!!!

using Microsoft.VisualBasic;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Window = System.Windows.Window;

namespace b51f01a
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public class Foo
        {
            public int Index { get; set; }

            public string Name { get; set; }

            public string Remark { get; set; }
        }

        public BackgroundWorker worker = new BackgroundWorker();

        public BackgroundWorker bw1 = new BackgroundWorker();

        private string cpuname = null;
//azz
        private const string _CPUPara = "Win32_Processor";

        private const string _CPUName = "Name";

        private const string _CPUIDPara = "ProcessorId";

        public int gd = 0;

        private string aaa = null;

        private string[] kl1 = File.ReadAllLines("data\\intro\\intro");

        private int ko = 0;


        public List<Foo> UserList { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            worker.WorkerReportsProgress = true;
            worker.DoWork += Worker_DoWork;
            worker.RunWorkerAsync();
            bw1.DoWork += Bw1_DoWork;
            HandyControl.Controls.MessageBox.Show("请标明原作者 hehe1008899 !! 禁止商业传播", "欢迎！", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        private void B2_Click(object sender, RoutedEventArgs e)
        {
            g1.Visibility = Visibility.Visible;
            g3.Visibility = Visibility.Hidden;
            g2.Visibility = Visibility.Hidden;
            g5.Visibility = Visibility.Hidden;
        }

        private void RichTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void G1_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                UserList = new List<Foo>
                {
                    new Foo
                    {
                        Index = 1,
                        Name = "macos11",
                        Remark = "link=none"
                    }
                };
            }
            ll1.ItemsSource = UserList;
        }

        private void Bw_Click(object sender, RoutedEventArgs e)
        {
            HandyControl.Controls.MessageBox.Show(File.ReadAllText("res\\text\\about.txt", Encoding.Default), "关于程序", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        private void G2_Initialized(object sender, EventArgs e)
        {
            //ManagementObjectSearcher query = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = 'TRUE'");
            //ManagementObjectCollection queryCollection = query.Get();
            //cpuname = GetCPUName();
            //rt2.AppendText("\nCPU型号: " + cpuname + "\n");
            //rt2.AppendText("内存大小: " + GetSystemMemorySizeOfGB() + " GB\n");
            //foreach (ManagementObject mo in queryCollection)
            //{
            //    rt2.AppendText("网卡信息: " + mo["Description"].ToString() + "\n");
            //}
            rt2.AppendText("显卡型号: " + GetGPUName() + "\n");
            rt2.AppendText("显存大小: " + GetGPUMemorySize() + " GB\n");
            rt2.AppendText("声卡型号: " + Getadu() + "\n");
            rt2.AppendText("声卡id: " + Getaduid() + "\n");
        }

        public string GetCPUName()
        {
            string str = null;
            //ManagementClass mcCPU = new ManagementClass("Win32_Processor");
            //ManagementObjectCollection mocCPU = mcCPU.GetInstances();
            //using (ManagementObjectCollection.ManagementObjectEnumerator managementObjectEnumerator = mocCPU.GetEnumerator())
            //{
            //    if (managementObjectEnumerator.MoveNext())
            //    {
            //        ManagementObject i = (ManagementObject)managementObjectEnumerator.Current;
            //        str = i["Name"].ToString();
            //    }
            //}
            return str;
        }

        public float GetSystemMemorySizeOfGB()
        {
            float size = 0f;
            //ManagementObjectSearcher searcher = new ManagementObjectSearcher();
            //searcher.Query = new SelectQuery("Win32_PhysicalMemory", "", new string[1] { "Capacity" });
            //ManagementObjectCollection collection = searcher.Get();
            //ManagementObjectCollection.ManagementObjectEnumerator em = collection.GetEnumerator();
            //long capacity = 0L;
            //while (em.MoveNext())
            //{
            //    ManagementBaseObject baseObj = em.Current;
            //    if (baseObj.Properties["Capacity"].Value != null)
            //    {
            //        capacity += long.Parse(baseObj.Properties["Capacity"].Value.ToString());
            //    }
            //}
            return size;
        }

        private void Rt2_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        public string GetGPUName()
        {
            string str = null;
            //ManagementClass manage = new ManagementClass("Win32_VideoController");
            //ManagementObjectCollection manageCollection = manage.GetInstances();
            //using (ManagementObjectCollection.ManagementObjectEnumerator managementObjectEnumerator = manageCollection.GetEnumerator())
            //{
            //    if (managementObjectEnumerator.MoveNext())
            //    {
            //        ManagementObject i = (ManagementObject)managementObjectEnumerator.Current;
            //        str = i["VideoProcessor"].ToString().Replace("Family", "");
            //    }
            //}
            return str;
        }

        public string Getadu()
        {
            string str = null;
            //ManagementClass manage = new ManagementClass("Win32_SoundDevice");
            //ManagementObjectCollection manageCollection = manage.GetInstances();
            //using (ManagementObjectCollection.ManagementObjectEnumerator managementObjectEnumerator = manageCollection.GetEnumerator())
            //{
            //    if (managementObjectEnumerator.MoveNext())
            //    {
            //        ManagementObject i = (ManagementObject)managementObjectEnumerator.Current;
            //        str = i["Name"].ToString();
            //    }
            //}
            return str;
        }

        public string Getaduid()
        {
            string str = null;
            //ManagementClass manage = new ManagementClass("Win32_SoundDevice");
            //ManagementObjectCollection manageCollection = manage.GetInstances();
            //using (ManagementObjectCollection.ManagementObjectEnumerator managementObjectEnumerator = manageCollection.GetEnumerator())
            //{
            //    if (managementObjectEnumerator.MoveNext())
            //    {
            //        ManagementObject i = (ManagementObject)managementObjectEnumerator.Current;
            //        str = i["DeviceID"].ToString().Remove(0, 32).Remove(4);
            //    }
            //}
            return str;
        }

        public float GetGPUMemorySize()
        {
            float size = 0f;
            //ManagementClass manage = new ManagementClass("Win32_VideoController");
            //ManagementObjectCollection manageCollection = manage.GetInstances();
            //using (ManagementObjectCollection.ManagementObjectEnumerator managementObjectEnumerator = manageCollection.GetEnumerator())
            //{
            //    if (managementObjectEnumerator.MoveNext())
            //    {
            //        ManagementObject i = (ManagementObject)managementObjectEnumerator.Current;
            //        size = Convert.ToInt64(i["AdapterRAM"]) / 1024 / 1024 / 1024;
            //    }
            //}
            return size;
        }

        public void check()
        {
            base.Dispatcher.Invoke(delegate
            {
                ll2.Items.Clear();
            });
            //FtpWebRequest ftp = (FtpWebRequest)WebRequest.Create("ftp://ftpupload.net//htdocs//efi");
            //ftp.Credentials = new NetworkCredential("epiz_29363612", "6mBRAAGlPi");
            //ftp.Method = "NLST";
            //WebResponse response = ftp.GetResponse();
            //StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.Default);
            //string line = reader.ReadLine();
            //while (line != null)
            //{
            //    if (!string.IsNullOrEmpty(line.Trim()) && !(line == ".") && !(line == ".."))
            //    {
            //        base.Dispatcher.Invoke(delegate
            //        {
            //            ll2.Items.Add(line);
            //        });
            //    }
            //    line = reader.ReadLine();
            //    base.Dispatcher.Invoke(delegate
            //    {
            //        ll2.SelectedIndex = 0;
            //    });
            //}
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            check();
            base.Dispatcher.Invoke(delegate
            {
                b1.IsEnabled = true;
                b2.IsEnabled = true;
                b3.IsEnabled = true;
            });
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            aaa = File.ReadAllText("data\\efi\\data");
            if (!(aaa != "") || !Directory.Exists("data\\efi\\" + aaa))
            {
                return;
            }
            b6.IsEnabled = false;
            Process proc = Process.Start("zip.exe", "unzip data\\efi\\" + aaa + "\\" + ll2.SelectedItem.ToString() + " data\\efi\\" + aaa + "\\");
            if (proc == null)
            {
                return;
            }
            proc.WaitForExit(3000);
            if (!proc.HasExited)
            {
                return;
            }
            string[] afg = File.ReadAllLines("data\\efi\\" + aaa + "\\prop.F", Encoding.Default);
            if (!(afg[7] == "clover"))
            {
                return;
            }
            ll3.Items.Clear();
            ll5.Items.Clear();
            gl5.Visibility = Visibility.Visible;
            string path = "data\\efi\\" + aaa + "\\config\\";
            DirectoryInfo root = new DirectoryInfo(path);
            FileInfo[] files = root.GetFiles();
            ll3.ItemsSource = files;
            refs();
            if (!File.Exists("ext\\extfile\\info.F"))
            {
                return;
            }
            string[] loi = File.ReadAllLines("ext\\extfile\\info.F", Encoding.Default);
            for (int i = 0; i <= loi.Length - 1; i++)
            {
                if (loi[i] == "<kext>")
                {
                    cmb1.Items.Add(loi[i + 1]);
                }
            }
            cmb1.SelectedIndex = 0;
        }

        public void refs()
        {
            string where = "data\\efi\\" + aaa + "\\EFI\\clover\\kexts\\other\\";
            DirectoryInfo rofs = new DirectoryInfo(where);
            DirectoryInfo[] fes1 = rofs.GetDirectories();
            ll5.ItemsSource = fes1;
        }

        private void Ll3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (File.Exists("data\\efi\\" + aaa + "\\EFI\\Clover\\config.plist"))
            {
                File.Delete("data\\efi\\" + aaa + "\\EFI\\Clover\\config.plist");
            }
            File.Copy("data\\efi\\" + aaa + "\\config\\" + ll3.SelectedItem.ToString(), "data\\efi\\" + aaa + "\\EFI\\Clover\\config.plist");
            showl("更换成功:" + ll3.SelectedItem.ToString());
        }

        private void B11_Click(object sender, RoutedEventArgs e)
        {
            if (ko == 0)
            {
                showw("请在选择文件完成后再点击一次扩展包安装按钮!");
                b11.Content = "点我安装";
                Process proc = Process.Start("fileselect.exe");
                ko = 1;
            }
            else
            {
                string path = File.ReadAllText("data\\fcdata\\fcdata", Encoding.Default);
                Process.Start("zip.exe", "unzip " + path + " ext\\extfile\\");
                ko = 0;
                b11.Content = "扩展包安装";
                showl("扩展包安装成功!");
            }
        }

        private void B12_Click(object sender, RoutedEventArgs e)
        {
            if (aaa != null)
            {
                showw("确保你的itlwm驱动已经添加到efi了!否则可能会出问题!");
                string uuid = Interaction.InputBox("请输入你的wifi账号", "itlwm配置 1.0.1");
                string passd = Interaction.InputBox("请输入你的wifi密码", "itlwm配置 1.0.1");
                string[] daft = File.ReadAllLines("data\\efi\\" + aaa + "\\EFI\\Clover\\kexts\\Other\\itlwm.kext\\Contents\\info.plist", Encoding.Default);
                daft[63] = "<string>" + passd + "<string>";
                daft[65] = "<string>" + uuid + "<string>";
                File.WriteAllLines("data\\efi\\" + aaa + "\\EFI\\Clover\\kexts\\Other\\itlwm.kext\\Contents\\info.plist", daft);
                showl("成功配置完成，文件请使用efi生成里的到处来到处");
            }
            else
            {
                showe("请确认{变量}有赋值!");
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Directory.CreateDirectory("data\\efi\\" + aaa + "\\EFI\\clover\\kexts\\other\\" + cmb1.SelectedItem.ToString());
            CopyDirectory("ext\\extfile\\" + cmb1.SelectedItem.ToString(), "data\\efi\\" + aaa + "\\EFI\\clover\\kexts\\other\\" + cmb1.SelectedItem.ToString() + "\\", overwriteexisting: true);
        }

        private bool CopyDirectory(string SourcePath, string DestinationPath, bool overwriteexisting)
        {
            bool ret = false;
            try
            {
                SourcePath = (SourcePath.EndsWith("\\") ? SourcePath : (SourcePath + "\\"));
                DestinationPath = (DestinationPath.EndsWith("\\") ? DestinationPath : (DestinationPath + "\\"));
                if (Directory.Exists(SourcePath))
                {
                    if (!Directory.Exists(DestinationPath))
                    {
                        Directory.CreateDirectory(DestinationPath);
                    }
                    string[] files = Directory.GetFiles(SourcePath);
                    foreach (string fls in files)
                    {
                        FileInfo flinfo = new FileInfo(fls);
                        flinfo.CopyTo(DestinationPath + flinfo.Name, overwriteexisting);
                    }
                    string[] directories = Directory.GetDirectories(SourcePath);
                    foreach (string drs in directories)
                    {
                        DirectoryInfo drinfo = new DirectoryInfo(drs);
                        if (!CopyDirectory(drs, DestinationPath + drinfo.Name, overwriteexisting))
                        {
                            ret = false;
                        }
                    }
                }
                ret = true;
            }
            catch (Exception)
            {
                ret = false;
            }
            showl("拷贝成功!");
            refs();
            return ret;
        }

        private void Cmb1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FlowDocument doc = rtp1.Document;
            doc.Blocks.Clear();
            rtp1.AppendText(kl1[cmb1.SelectedIndex]);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (tt1.Text != "")
            {
                if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + tt1.Text))
                {
                    Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + tt1.Text);
                }
                else
                {
                    showe("文件夹已经存在了!");
                }
                if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + tt1.Text + "\\EFI"))
                {
                    Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + tt1.Text + "\\EFI");
                }
                CopyDirectory("data\\efi\\" + aaa + "\\EFI", Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + tt1.Text + "\\EFI\\", overwriteexisting: true);
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            if (ll5.SelectedItem.ToString() != "VirtualSMC.kext" && ll5.SelectedItem.ToString() != "WhateverGreen.kext" && ll5.SelectedItem.ToString() != "Lilu.kext")
            {
                DirectoryInfo di = new DirectoryInfo("data\\efi\\" + aaa + "\\EFI\\clover\\kexts\\other\\" + ll5.SelectedItem.ToString());
                di.Delete(recursive: true);
                showl("删除成功");
                refs();
            }
            else
            {
                showe("基本的内核驱动是不可以删除的哦!");
            }
        }

        private void Window_PreviewMouseMove(object sender, MouseEventArgs e)
        {
        }

        private void Top_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void B5_Click(object sender, RoutedEventArgs e)
        {
        }

        private void N1_Click(object sender, RoutedEventArgs e)
        {
            NotifyIcon.ShowBalloonTip("当前没有正在运行的任务", "当前没有正在运行的任务", NotifyIconInfoType.Info, "当前没有正在运行的任务");
        }



        private void Top_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Bcl_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            debug.Visibility = Visibility.Visible;
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            g1.Visibility = Visibility.Visible;
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            g2.Visibility = Visibility.Visible;
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            g3.Visibility = Visibility.Visible;
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            g5.Visibility = Visibility.Visible;
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            debug.Visibility = Visibility.Hidden;
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            g1.Visibility = Visibility.Hidden;
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {
            g2.Visibility = Visibility.Hidden;
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            g3.Visibility = Visibility.Hidden;
        }

        private void Button_Click_15(object sender, RoutedEventArgs e)
        {
            g5.Visibility = Visibility.Hidden;
        }

        private void Button_Click_16(object sender, RoutedEventArgs e)
        {
            gl5.Visibility = Visibility.Visible;
        }

        private void Button_Click_17(object sender, RoutedEventArgs e)
        {
            gl5.Visibility = Visibility.Hidden;

        }
    }
}
