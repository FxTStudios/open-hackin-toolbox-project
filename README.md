# OpenHackinToolboxProject

#### 介绍
OpenHackinToolboxProject,fbtstudio的HackinToolboxProject的开源实现版本，仅包含部分代码
代码是逆向工程所得，但是在官方授权的情况下进行的逆向工程，并非侵犯他人利益和著作权
本项目使用BSD协议开源！

#### 安装教程

请在发行版中下载您需要的版本，然后解压运行

#### 项目特别说明

虽然本项目是开源的项目，但是内部反编译的代码仅仅是部分，而且我们这边在
发行版中发布的编译好的版本都是有加壳保护的，禁止进行二次逆向

#### 参与贡献

项目负责人&编写 lajiaoyou
项目管理者 hehe1008899
原开发者 hehe1008899
项目团队 fbtstudio
特别感谢 TBS

